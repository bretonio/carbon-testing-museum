import Vue from 'vue'
import App from './App.vue'
import router from './router'
import './registerServiceWorker'

import { theme } from 'muse-ui'
import { carbon, createTheme } from 'muse-ui-carbon-theme';
import MuseUI from 'muse-ui'

theme.add('carbon', carbon)
  .addCreateTheme(createTheme)
  .use('carbon');

Vue.use(MuseUI)

Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
